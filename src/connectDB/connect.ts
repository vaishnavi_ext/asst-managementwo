import mongoose from "mongoose";

export const connectDB = async (url: string)=>{
    mongoose.set('strictQuery', false)
   return await mongoose.connect(url).then(()=>console.log("Connected to the database")).catch((err)=> console.log(err))
}

