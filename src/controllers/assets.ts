import { Request, Response } from "express";
import Asset from "../models/assets.model";
import Employee from "../models/employee.model";
//all assets

export const getAllAssets = async (req: Request, res: Response) => {
  try {
    let data = req.query;
    const { assetType, os } = data;
    let queryObj: any = {};
    if (assetType) {
      queryObj.assetType = assetType;
    }
    if (os) {
      queryObj.os = os;
    }
    const token = req.headers.token;
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    // if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
    //   return res.status(401).json({ message: "unauthorised user" });
    // }

    let result = Asset.find(queryObj).populate("assetType");
    const allAssets = await result;
    if (employeeRole.role === "manager" && employeeRole.role === "employee") {
      const mobiles = allAssets.filter((mobiles) => {
        let m = mobiles.assetType;
        console.log(m);
      });
      console.log(mobiles);
    }
    if (!allAssets) {
      return res.status(404).json({ message: "No Assets found!" });
    } else {
      return res.status(200).json({ message: "Here's the list", allAssets });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const createAsset = async (req: Request, res: Response) => {
  try {
    const token = req.headers.token;
    const data = req.body;
    const {
      serialNumber,
      modelNumber,
      modelName,
      image,
      ram,
      processor,
      internalStorage,
      keyboard,
      mouse,
      assetType,
      imeinumber,
      os,
      otherSpecs,
      id,
    } = data;
    let value = Math.floor(Math.random() * 1000 + 1000);
    const assertData = await Asset.find({ serialNumber: serialNumber });
    if (assertData.length > 0) {
      return res.status(403).json({ message: "Asset details already exists" });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    const createDetails = await Asset.create({
      serialNumber: serialNumber,
      modelNumber: modelNumber,
      modelName: modelName,
      ram: ram,
      assetType: assetType,
      processor: processor,
      internalStorage: internalStorage,
      image: image,
      keyboard: keyboard,
      mouse: mouse,
      imeinumber: imeinumber,
      os: os,
      otherSpecs: otherSpecs,
      id: value,
    });

    if (createDetails)
      return res
        .status(200)
        .json({ message: "created asset details!", createDetails });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const updateAssetDetails = async (req: Request, res: Response) => {
  try {
    const token = req.headers.token;
    const { id } = req.params;
    const {
      serialNumber,
      modelNumber,
      modelName,
      image,
      ram,
      processor,
      internalStorage,
      keyboard,
      mouse,
      assetType,
      mobileOS,
      otherSpecs,
    } = req.body;
    const data = await Asset.findOne({ _id: id });
    if (!data) {
      return res.status(404).json({ message: "Asset ID not found!" });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    const updatedDetails = await data.updateOne({
      serialNumber: serialNumber,
      modelNumber: modelNumber,
      modelName: modelName,
      ram: ram,
      assetType: assetType,
      processor: processor,
      internalStorage: internalStorage,
      image: image,
      keyboard: keyboard,
      mouse: mouse,
      mobileOS: mobileOS,
      otherSpecs: otherSpecs,
    });
    return res
      .status(200)
      .json({ message: "Details updated successfully!", updatedDetails });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const getAssetsById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = await Asset.findOne({ _id: id });
    return res.status(200).json({ message: "Found!", data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const deleteAssetDetails = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const token = req.headers.token;
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    const getDetails = await Asset.deleteOne({ _id: id });
    if (getDetails.deletedCount > 0) {
      return res.status(200).json({ message: "Deleted!", getDetails });
    }
    return res.status(404).json({ message: "No device found!" });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
//laptop
("You are it");
// export const createLaptopDetails = async (req: Request, res: Response) => {
//   try {
//     const data = req.body;
//     const {
//       serialNumber,
//       modelNumber,
//       modelName,
//       device,
//       image,
//       ram,
//       processor,
//       internalStorage,
//     } = data;
//     const laptopData = await Asset.find({ serialNumber: serialNumber });
//     if (laptopData.length > 0) {
//       return res.status(400).json({ message: "Laptop details already exists" });
//     }
//     const createDetails = await Asset.create({
//       serialNumber: serialNumber,
//       modelNumber: modelNumber,
//       modelName: modelName,
//       ram: ram,
//       device: device,
//       processor: processor,
//       internalStorage: internalStorage,
//       image: image,
//     });

//     return res
//       .status(200)
//       .json({ message: "created laptop details!", createDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const getLaptopDetails = async (req: Request, res: Response) => {
//   try {
//     const getDetails = await Asset.find({});
//     return res.status(200).json({ message: "found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const getLaptopDetailsById = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.findOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const updateLaptopDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const {
//       serialNumber,
//       modelNumber,
//       modelName,
//       image,
//       device,
//       ram,
//       processor,
//       internalStorage,
//     } = req.body;
//     const updateDetails = await Asset.findOne({ _id: id });
//     if (!updateDetails) {
//       return res.status(400).json({ message: "Laptop ID not found!" });
//     }
//     const updatedDetails = await updateDetails.updateOne({
//       serialNumber: serialNumber,
//       modelNumber: modelNumber,
//       modelName: modelName,
//       ram: ram,
//       device: device,
//       processor: processor,
//       internalStorage: internalStorage,
//       image: image,
//     });
//     return res
//       .status(400)
//       .json({ message: "Details updated successfully!", updatedDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const deleteLaptopDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.deleteOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Deleted!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// //desktop

// export const createDesktopDetails = async (req: Request, res: Response) => {
//   try {
//     const data = req.body;
//     const {
//       serialNumber,
//       modelNumber,
//       modelName,
//       image,
//       ram,
//       processor,
//       internalStorage,
//       keyboard,
//       mouse,
//       device,
//     } = data;
//     const desktopData = await Asset.find({ serialNumber: serialNumber });
//     if (desktopData.length > 0) {
//       return res
//         .status(400)
//         .json({ message: "Desktop details already exists" });
//     }
//     const createDetails = await Asset.create({
//       serialNumber: serialNumber,
//       modelNumber: modelNumber,
//       modelName: modelName,
//       ram: ram,
//       device: device,
//       processor: processor,
//       internalStorage: internalStorage,
//       image: image,
//       keyboard: keyboard,
//       mouse: mouse,
//     });
//     return res
//       .status(200)
//       .json({ message: "created desktop details!", createDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const getDesktopDetails = async (req: Request, res: Response) => {
//   try {
//     const getDetails = await Asset.find({});
//     return res.status(200).json({ message: "found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };
// export const getDesktopDetailsById = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.findOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const updateDesktopDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const {
//       serialNumber,
//       modelNumber,
//       modelName,
//       image,
//       ram,
//       processor,
//       internalStorage,
//       keyboard,
//       mouse,
//       device,
//     } = req.body;
//     const updateDetails = await Asset.findOne({ _id: id });
//     if (!updateDetails) {
//       return res.status(400).json({ message: "Desktop ID not found!" });
//     }
//     const updatedDetails = await updateDetails.updateOne({
//       serialNumber: serialNumber,
//       modelNumber: modelNumber,
//       modelName: modelName,
//       ram: ram,
//       processor: processor,
//       internalStorage: internalStorage,
//       image: image,
//       keyboard: keyboard,
//       mouse: mouse,
//       device: device,
//     });
//     return res
//       .status(400)
//       .json({ message: "Details updated successfully!", updatedDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const deleteDesktopDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.deleteOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Deleted!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// //mobile

// export const createMobileDetails = async (req: Request, res: Response) => {
//   try {
//     const data = req.body;
//     const {
//       serialNumber,
//       imeinumber,
//       modelName,
//       ram,
//       device,
//       mobileOS,
//       internalStorage,
//       image,
//     } = data;
//     const mobileData = await Asset.find({ serialNumber: serialNumber });
//     if (mobileData.length > 0) {
//       return res.status(400).json({ message: "Mobile details already exists" });
//     }
//     const createDetails = await Asset.create({
//       serialNumber: serialNumber,
//       imeinumber: imeinumber,
//       modelName: modelName,
//       device: device,
//       ram: ram,
//       mobileOS: mobileOS,
//       internalStorage: internalStorage,
//       image: image,
//     });
//     return res
//       .status(200)
//       .json({ message: "created mobile details!", createDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const getMobileDetails = async (req: Request, res: Response) => {
//   try {
//     const getDetails = await Asset.find({});
//     return res.status(200).json({ message: "found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const getMobileDetailsById = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.findOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Found!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const updateMobileDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const {
//       serialNumber,
//       imeinumber,
//       modelName,
//       mobileOS,
//       ram,
//       device,
//       internalStorage,
//       image,
//     } = req.body;
//     const updateDetails = await Asset.findOne({ _id: id });
//     if (!updateDetails) {
//       return res.status(400).json({ message: "Mobile ID not found!" });
//     }
//     const updatedDetails = await updateDetails.updateOne({
//       serialNumber: serialNumber,
//       imeinumber: imeinumber,
//       modelName: modelName,
//       ram: ram,
//       device: device,
//       mobileOS: mobileOS,
//       internalStorage: internalStorage,
//       image: image,
//     });
//     return res
//       .status(400)
//       .json({ message: "Details updated successfully!", updatedDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };

// export const deleteMobileDetails = async (req: Request, res: Response) => {
//   try {
//     const { id } = req.params;
//     const getDetails = await Asset.deleteOne({ _id: id });
//     if (!getDetails) {
//       return res.status(200).json({ message: "Device not found!" });
//     }
//     return res.status(200).json({ message: "Deleted!", getDetails });
//   } catch (error) {
//     return res.status(400).json({ message: error });
//   }
// };
