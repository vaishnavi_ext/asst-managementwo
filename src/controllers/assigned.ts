import { Response, Request } from "express";
import userAssigned from "../models/assigned.model";
import Asset from "../models/assets.model";
import Employee from "../models/employee.model";

export const assignedUser = async (req: Request, res: Response) => {
  try {
    const token = req.headers.token;
    const { employeeDetails, assetNumber } = req.body;
    const id = req.body.userId;
    const details = await Employee.findOne({ id });
    console.log(details.id);
    console.log(employeeDetails);

    if (employeeDetails !== details.id) {
      return res
        .status(400)
        .json({ message: "employee details and id not matched" });
    }
    const data = await userAssigned.create({
      employeeDetails,
      assetNumber,
      deliveryStatus: "picked from the location",
      trackingId: "default",
      deliveryCompany: "One",
      remark: "Order started",
    });
    if (!employeeDetails || !assetNumber) {
      return res.status(400).json({ message: "All fields are required!" });
    }
    return res.status(200).json({ message: data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const updateDetails = async (req: Request, res: Response) => {
  try {
    const { deliveryStatus, trackingId, deliveryCompany, remark } = req.body;
    const token = req.headers.token;
    const id = req.params;
    const data = await userAssigned.findOne({ _id: id });
    if (!data) {
      return res.status(400).json({ message: "No asset details found" });
    }
    const updated = await data.update({
      deliveryStatus: deliveryStatus,
      trackingId: trackingId,
      deliveryCompany: deliveryCompany,
      remark: remark,
    });
    if (!deliveryStatus || !trackingId || !deliveryCompany || !remark) {
      return res.status(400).json({ message: "Enter the details" });
    }
    return res
      .status(200)
      .json({ message: "Updated the delivery status", updated });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
export const getData = async (req: Request, res: Response) => {
  try {
    // const user = await Employee.findOne({ employeeId });
    //(if user.location === work from home ){
    // const {trackingId, employeeId} = req.body
    //}
    const token = req.headers.token;
    const data = await userAssigned
      .find({})
      .populate({ path: "assetNumber" })
      .populate({ path: "employeeDetails", select: ["name", "status"] });
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    return res.status(200).json({ message: data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
