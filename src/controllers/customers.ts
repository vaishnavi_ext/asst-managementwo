import e, { Request, Response } from "express";
import jwt from "jsonwebtoken";
// import generateId from "../utils/generateId";
import customerModel from "../models/customer.model";
import * as config from '../utils/config'

export const getOtp = async (req: Request, res: Response) => {
  try {
    const { mobile } = req.body;
    const user = await customerModel.findOne({ mobile });
    const otp = Math.floor(1000 + Math.random() * 9000).toString();
    console.log(otp)
    if (user) {
      user.otp = otp;
      user.updateOtp = new Date();
      const result = await user.save();
      res
        .status(200)
        .json(result.toJSON());
    } else {
      const customerId = "" //await generateId('CUSTOMER')
      const data = {
        mobile,
        createdAt: new Date(),
        otp: otp,
        updateOtp: new Date(),
        customerId,
      };
      const userInstance = new customerModel(data);

      const result = await userInstance.save();

      res.status(200).json(result.toJSON());
    }
  } catch (e) {
    res.status(500).json({ message: "Something went wrong!", error: e });
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { mobile, otp, email, firstname, lastname } = req.body;
    const user = await customerModel.findOne({ mobile });
    if (user) {
      if (otp === user.otp) {
        const token = jwt.sign({mobile, email, firstname, lastname, type: 'customer', id: user._id}, config.JWT_SECRET, {
          expiresIn: config.JWT_EXPIRED_IN,
        });

        res
          .status(200)
          .json({ ...user.toJSON(), token });
      } else {
        res
          .status(401)
          .json({ status: false, message: "You have entered wrong otp" });
      }
    } else {
      res
          .status(404)
          .json({ status: false, message: "User not found!" });
    }
  } catch (e) {
    res.status(500).json({ message: "Something went wrong!", error: e });
  }
};

export const updateProfile = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const user = await customerModel.findOne({ _id: id });
    if (user) {
      delete req.body.mobile
      delete req.body._id
      delete req.body.customerId

      await user.updateOne(req.body);
      const updatedData = await customerModel.findOne({ _id: id });
      res.status(200).json(updatedData.toJSON());
    } else {
      res.status(404).json({ status: false, message: "Cannot find id" });
    }
  } catch (e: any) {
    res
      .status(500)
      .json({ status: false, message: "Something went wrong!", error: e });
  }
};


export const getProfileDetails = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const customer = await customerModel.findOne({ _id: id });
    if (customer) {
      res.status(200).json(customer.toJSON());
    } else {
      res.status(404).json({ status: false, message: "Cannot find id" });
    }
  } catch (e: any) {
    res
      .status(500)
      .json({ status: false, message: "Something went wrong!", error: e });
  }
};