import express, { Request, Response } from "express";
import multer from "multer";

const router = express.Router();

router.use(
  express.urlencoded({
    limit: "50mb",
    extended: true,
  })
);

router.use(
  express.json({
    limit: "50mb",
  })
);

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({
  storage,
  limits: { fieldSize: 50 * 1024 * 1024 },
});

router.use(express.static("public"));

router.post("/", upload.single("image"), (req, res) => {
  if (req.file) return res.json(req.file);
  else {
    res.status(409).json({
      statusCode: 409,
      message: "No Files to Upload.",
    });
  }
});

export default router;
