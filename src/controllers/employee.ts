import { Request, Response } from "express";
import Employee from "../models/employee.model";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { mailer } from "../utils/otpGen";

export const createEmployeeDetails = async (req: Request, res: Response) => {
  try {
    let data = req.body;
    let { employeeId, name, emailId, role } = data;
    //to hash the password
    const employeeIdData = await Employee.find({ employeeId: employeeId });
    if (employeeIdData.length > 0) {
      return res
        .status(403)
        .json({ message: "employee already exists", employeeIdData });
    }
    // const phoneNumber = await Employee.findOne({ mobileNumber: mobileNumber });
    // if (phoneNumber) {
    //   return res
    //     .status(403)
    //     .json({ message: "phone number already exists", employeeIdData });
    // }
    const emailAddress = await Employee.findOne({ emailId: emailId });
    if (emailAddress) {
      return res.status(403).json({ message: "emailId already exists" });
    }
    if (!role) {
      return res.status(403).json({ message: "Role is required" });
    }
    //to add token
    const employeeData = await Employee.create({
      employeeId: employeeId,
      password: "extended_it",
      confirmPassword: "extended_it",
      name: name,
      // mobileNumber: mobileNumber,
      emailId: emailId,
      role: role,
      // address: address,
      // shippingAddress: shippingAddress,
      // location: location,
      // status: "active",
    });
    return res
      .status(200)
      .json({ message: "employee added successfully", employeeData });
  } catch (error) {
    res.status(400).json({ message: "something went wrong", error: error });
  }
};

// login with the created employee details
export const loginDetails = async (req: Request, res: Response) => {
  try {
    let data = req.body;
    const { employeeId, password } = data;
    const employeeDetails = await Employee.findOne({ employeeId });
    const role = employeeDetails.role;
    if (!employeeDetails) {
      return res.status(404).json({ message: "Employee not found!" });
    }
    if (!(await bcrypt.compare(password, employeeDetails.password))) {
      return res.status(400).json({ message: "Incorrect password!" });
    }
    if (employeeDetails.status === "inactive") {
      return res.status(400).json({ message: "account inactive!" });
    }
    if (
      employeeDetails &&
      (await bcrypt.compare(password, employeeDetails.password)) &&
      employeeDetails.status === "active"
    ) {
      const token = jwt.sign(
        { employeeId: employeeDetails.employeeId },
        process.env.KEY
      );
      return res
        .status(200)
        .json({ message: "Logged In successfully!", token, role });
    }
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};
//updating details - can only update the password
export const updateDetails = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = req.body;
    let {
      name,
      mobileNumber,
      emailId,
      address,
      shippingAddress,
      location,
      status,
      password,
      confirmPassword,
    } = data;
    // let salt = await bcrypt.genSalt(10);
    // let encryptedPassword = await bcrypt.hash(password, salt); //.hash(password, salts)
    let encryptedPassword = await bcrypt.hash(password, 5);
    const details = await Employee.findOne({ _id: id });
    if (details) {
      const updated = await details.updateOne({
        name: name,
        password: encryptedPassword,
        confirmPassword: encryptedPassword,
        mobileNumber: mobileNumber,
        emailId: emailId,
        address: address,
        shippingAddress: shippingAddress,
        location: location,
        status: status,
      });
      return res
        .status(200)
        .json({ message: "Details updated successfully!", updated });
    }
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getEmployee = async (req: Request, res: Response) => {
  try {
    const employees = await Employee.find({}).sort({
      employeeId: "asc",
    });
    if (employees) {
      res.status(200).json(employees);
    } else {
      res.status(404).json({ status: false, message: "No Employees found!" });
    }
  } catch (e: any) {
    res
      .status(500)
      .json({ status: false, message: "Something went wrong!", error: e });
  }
};

export const deleteEmployee = async (req: Request, res: Response) => {
  try {
    const { employeeId } = req.params;
    const deleteEmployee = await Employee.deleteOne({ employeeId });
    if (deleteEmployee.deletedCount > 0) {
      return res.status(200).json({ message: "Employee deleted!" });
    } else {
      return res.status(404).json({ message: "Employee ID not found!" });
    }
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

export const getEmployeeById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    // const token: any = req.headers.token;
    const employee: any = await Employee.findOne({
      _id: id,
    }).catch(() => {});

    // if (employee.token !== token) {
    //   return res.status(400).json({ message: "invalid token" });
    // }
    if (employee) {
      res.status(200).json(employee);
    } else {
      res
        .status(404)
        .json({ status: false, message: "Cannot find employee details" });
    }
  } catch (e: any) {
    res
      .status(500)
      .json({ status: false, message: "Something went wrong!", error: e });
  }
};

// to update forgot password with an otp
export const genOtp = async (req: Request, res: Response) => {
  try {
    const { emailId } = req.body;
    const addOtp = await Employee.findOne({ emailId: emailId });
    if (!addOtp) {
      return res.status(400).json({ message: "Employee not found" });
    }
    const token = jwt.sign({ emailId: emailId }, process.env.KEY);
    const otp = Math.floor(1000 + Math.random() * 9000);
    const getOtp: any = mailer(addOtp.emailId, otp);
    addOtp.otp = otp;
    await addOtp.save();
    return res.status(200).send({
      status: true,
      msg: "otp sent successfully",
      // data: addOtp._id,
      otp,
      token,
    });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
};

export const verifyOtp = async (req: Request, res: Response) => {
  try {
    const { otp } = req.body;
    const id = req.body.userId;
    const employee = await Employee.findOne({ emailId: id });
    console.log(employee);
    console.log("id", id);
    if (employee.emailId !== id) {
      return res.status(400).json({ message: "Invalid credentials" });
    }
    if (!employee) {
      return res
        .status(404)
        .send({ status: false, message: "Employee not found" });
    }
    console.log(employee.otp);
    if (otp !== employee.otp) {
      return res.status(400).send({ status: false, message: "Incorrect otp" });
    }
    return res.status(201).json({
      message: "OTP verified successfully",
      data: otp,
    });
  } catch (err) {
    return res.status(500).send({ status: false, message: err.message });
  }
};

export const forgotPassword = async (req: Request, res: Response) => {
  try {
    const { employeeId, password, confirmPassword } = req.body;
    const id = req.body.userId;
    const findEmployee = await Employee.findOne({ employeeId: id });
    if (!findEmployee) {
      return res
        .status(404)
        .send({ status: false, message: "Employee not found" });
    }
    if (employeeId !== id) {
      return res.status(400).json({ message: "Invalid credentials" });
    }
    if (employeeId != findEmployee.employeeId) {
      return res.status(403).send({ status: false, msg: "Not authorised" });
    }

    if (password !== confirmPassword) {
      return res
        .status(400)
        .send({ status: false, message: "Passwords are not matching" });
    }
    let encryptedPassword = await bcrypt.hash(password, 5);
    const updatedPassword = await Employee.findOneAndUpdate(
      { employeeId: employeeId },
      {
        $set: {
          password: encryptedPassword,
          confirmPassword: encryptedPassword,
        },
      },
      { new: true }
    );
    let data = {
      employeeId: updatedPassword.employeeId,
      password: updatedPassword.password,
    };
    return res.status(200).send({ status: true, msg: "Password updated" });
  } catch (err) {
    return res.status(500).send({ status: false, msg: err.message });
  }
};

export const statusData = async (req: Request, res: Response) => {
  try {
    const { employeeId, status } = req.body;
    const findAsset = await Employee.findOne({
      employeeId: employeeId,
    });
    if (!findAsset) {
      return res
        .status(404)
        .send({ status: false, message: "Employee not found" });
    }
    const updateStatus = await findAsset.updateOne({ status: status });
    updateStatus.status = status;
    return res
      .status(200)
      .send({ status: true, msg: "status changed", updateStatus });
  } catch (error) {
    return res.status(500).json({ status: false, msg: error });
  }
};
