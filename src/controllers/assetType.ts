import { Response, Request } from "express";
import assetType from "../models/assetType.model";
import Employee from "../models/employee.model";

export const createAssertType = async (req: Request, res: Response) => {
  try {
    const data = req.body;
    const token = req.headers.token;
    const { name } = data;
    const findType = await assetType.findOne({ name });
    if (findType) {
      return res.status(403).json({ message: "Already exists!" });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    const assertTypeDetails = await assetType.create({ name: name });
    return res.status(200).json({ message: "Created!", assertTypeDetails });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getAssetTypes = async (req: Request, res: Response) => {
  try {
    const token = req.headers.token;
    const data = await assetType.find({});
    if (!data) {
      return res.status(404).json({ message: "No details found!" });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    return res.status(200).json({ data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const getAssetsById = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const data = await assetType.findOne({ _id: id });
    if (!data) {
      return res.status(404).json({ message: "Device not found!" });
    }
    return res.status(200).json({ message: "Found!", data });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const updateAssetTypes = async (req: Request, res: Response) => {
  try {
    const id = req.params;
    const token = req.headers.token;
    const { name } = req.body;
    const data = await assetType.find({ _id: id });
    if (!data) {
      return res.status(404).json({ message: "No details found!" });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    const updateType = await assetType.updateOne({ name: name });
    return res
      .status(200)
      .json({ message: "Details updated successfully!", updateType });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};

export const deleteAssetType = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const token = req.headers.token;
    const data = await assetType.deleteOne({ _id: id });
    if (data.deletedCount > 0) {
      return res.status(200).json({ message: "Deleted!", data });
    }
    const employeeRole: any = await Employee.findOne({ token });
    if (!token) {
      return res.status(400).json({ message: "invalid token" });
    }
    if (employeeRole.role !== "admin" && employeeRole.role !== "it") {
      return res.status(401).json({ message: "unauthorised user" });
    }
    return res.status(404).json({ message: "Device not found!" });
  } catch (error) {
    return res.status(400).json({ message: error });
  }
};
