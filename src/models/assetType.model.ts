import { Schema, model } from "mongoose";

interface AssertType {
  name: string;
}

const assetType = new Schema(
  {
    name: {
      type: String,
    },
  },
  { versionKey: false }
);

assetType.method("toJSON", function () {
  let obj: any = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  return obj;
});

const types = model<AssertType>("assetType", assetType);
export default types;
