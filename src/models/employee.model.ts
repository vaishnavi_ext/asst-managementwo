import { TokenExpiredError } from "jsonwebtoken";
import mongoose, { model, Schema } from "mongoose";

interface Employee {
  name: string;
  employeeId: string;
  mobileNumber: string;
  emailId: string;
  role: string;
  password: string;
  confirmPassword: string;
  address: string;
  shippingAddress: string;
  location: string;
  token: string;
  otp: number;
  status: string;
  assetType: string;
}
const employeeSchema = new Schema(
  // name, emp id, phone num, email id, role, password, confirm password, address, shipping address  - pincode, land mark, street, city, state.
  {
    name: {
      type: String,
    },
    employeeId: {
      type: String,
    },
    mobileNumber: {
      type: String,
    },
    emailId: {
      type: String,
    },
    role: {
      type: String,
      enum: {
        values: ["employee", "admin", "manager", "it"],
      },
    },
    password: {
      type: String,
    },
    confirmPassword: {
      type: String,
    },
    address: {
      type: String,
    },
    shippingAddress: {
      type: String,
    },
    location: {
      type: String,
      enum: {
        values: ["work from office", "work from home"],
      },
    },
    otp: {
      type: Number,
    },
    status: {
      type: String,
      enum: {
        values: ["Approved", "Rejected", "active", "inactive"],
      },
    },
    token: {
      type: String,
    },
    assetType: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "assetType",
    },
  },
  { timestamps: true, versionKey: false }
);

employeeSchema.method("toJSON", function () {
  let obj: any = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  delete obj.password;
  delete obj.confirmPassword;
  delete obj.otp;
  return obj;
});

const employee = model<Employee>("employee", employeeSchema);
export default employee;
