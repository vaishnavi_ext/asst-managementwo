import mongoose, { model, Schema } from "mongoose";

interface Assigned {
  assetNumber: number;
  employeeDetails: string;
  trackingId: string;
  deliveryStatus: string;
  deliveryCompany: string;
}
const allotSchema = new Schema(
  {
    assetNumber: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "asserts",
    },
    employeeDetails: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "employee",
    },
    trackingId: {
      type: String,
    },
    deliveryStatus: {
      type: String,
      enum: {
        values: ["delivered", "picked from the location", "on the way!"],
      },
    },
    remark: {
      type: String,
    },
    deliveryCompany: {
      type: String,
    },
  },
  { timestamps: true, versionKey: false }
);

allotSchema.method("toJSON", function () {
  let obj: any = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  return obj;
});

const assigned = model<Assigned>("assigned", allotSchema);
export default assigned;
