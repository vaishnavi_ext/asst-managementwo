import mongoose, { Schema, model } from "mongoose";

interface Asset {
  serialNumber: string;
  modelNumber: string;
  modelName: string;
  assetType: String;
  ram: string;
  processor: string;
  internalStorage: string;
  image: string;
  keyboard: string;
  mouse: string;
  os: string;
  otherSpecs: string;
}

const assetSchema = new Schema(
  {
    serialNumber: {
      type: String,
    },
    modelName: {
      type: String,
    },
    modelNumber: {
      type: String,
    },
    ram: {
      type: String,
    },
    assetType: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "assetType",
    },
    processor: {
      type: String,
    },
    internalStorage: {
      type: String,
    },
    keyboard: {
      type: String,
    },
    mouse: {
      type: String,
    },
    image: {
      type: String,
    },
    os: {
      type: String,
    },
    otherSpecs: {
      type: String,
    },
    id: {
      type: String,
    },
  },
  { timestamps: true, versionKey: false }
);

assetSchema.method("toJSON", function () {
  let obj: any = this.toObject();
  obj.id = obj._id;
  delete obj._id;
  return obj;
});

const assets = model<Asset>("asserts", assetSchema);
export default assets;
