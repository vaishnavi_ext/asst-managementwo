import express from "express";
const path = require("path");
// import customerAuthRoute from "./routes/customer.route";
import employeeRoute from "./routes/employee.route";
import upload from "./controllers/upload";
import cors from "cors";
import dotenv from "dotenv";
import { connectDB } from "../src/connectDB/connect";
import bodyParser from "body-parser";
import assetRoute from "./routes/asset.route";
import assetType from "./routes/assetType.route";
import assignedRoute from "./routes/assigned.route";
const port = process.env.PORT || 8080;
dotenv.config();

//initial app
const app = express();

app.use(cors({ origin: "*" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

// app.use("/customer", customerAuthRoute);
app.use("/employee", employeeRoute);
app.use("/upload", upload);
app.use("/assets", assetRoute);
app.use("/asserttype", assetType);
app.use("/assigned", assignedRoute);
// catch 404 and forward to error handler
app.use((req, res) => {
  res.status(404).json({ message: "Page not found!" });
});

// error handler
app.use((err: any, req: any, res: any, next: any) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  res.status(err.status || 500).json({ message: "bad request!", err });
});

const start = () => {
  try {
    connectDB(process.env.URL);
    app.listen(port, function () {
      console.log("Server started at this port:", port);
    });
  } catch (error) {
    console.error(error);
  }
};

start();
