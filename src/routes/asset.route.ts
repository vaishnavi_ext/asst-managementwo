import * as controller from "../controllers/assets";
import { assetValidation } from "../utils/assetValidation";
import { tokenValidation } from "../utils/validateToken";
import { isUser, isIT, isAdmin } from "../utils/roleValidation";
const express = require("express");
const router = express.Router();

router.get("/", tokenValidation, isAdmin, controller.getAllAssets);
router.post("/create", tokenValidation, controller.createAsset);
router.get("/:id", tokenValidation, controller.getAssetsById);
router.patch(
  "/:id",
  tokenValidation,
  assetValidation,
  controller.updateAssetDetails
);
router.delete("/:id", tokenValidation, controller.deleteAssetDetails);
//mobile routes
// router.post("/mobile", mobileValidation, createMobileDetails);
// router.get("/mobile", getMobileDetails);
// router.get("/mobile/:id", getMobileDetailsById);
// router.patch("/mobile/:id", mobileValidation, updateMobileDetails);
// router.delete("/mobile/:id", deleteMobileDetails);

// // desktop routes
// router.post("/desktop", desktopValidation, createDesktopDetails);
// router.get("/desktop", getDesktopDetails);
// router.get("/desktop/:id", getDesktopDetails);
// router.patch("/desktop/:id", desktopValidation, updateDesktopDetails);
// router.delete("/desktop/:id", deleteDesktopDetails);

export default router;
