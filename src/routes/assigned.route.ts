const express = require("express");
const router = express.Router();
import * as controller from "../controllers/assigned";

router.post("/", controller.assignedUser);
router.post("/:id", controller.updateDetails);
router.get("/", controller.getData);

export default router;
