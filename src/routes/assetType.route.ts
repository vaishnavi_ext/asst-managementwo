import * as controller from "../controllers/assetType";
import { tokenValidation } from "../utils/validateToken";
const express = require("express");
const router = express.Router();
import { assetTypeValidation } from "../utils/assetTypeValidation";

router.post(
  "/create",
  assetTypeValidation,
  tokenValidation,
  controller.createAssertType
);
router.get("/", tokenValidation, controller.getAssetTypes);
router.patch(
  "/:id",
  tokenValidation,
  assetTypeValidation,
  controller.updateAssetTypes
);
router.delete("/:id", tokenValidation, controller.deleteAssetType);
router.get("/:id", tokenValidation, controller.getAssetsById);

export default router;
