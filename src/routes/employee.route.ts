import express from "express";
import * as controller from "../controllers/employee";
import {
  employeeValidation,
  updateDetailsVerify,
} from "../utils/employeeValdation";
import { getOtpValidation, loginValidation } from "../utils/otpValidation";
import { tokenValidation } from "../utils/validateToken";
import { isUser, isAdmin, isIT, isManager } from "../utils/roleValidation";

const router = express.Router();

//get all category
router.get("/details", tokenValidation, isIT, controller.getEmployee);
router.get("/details/:id", tokenValidation, controller.getEmployeeById);

//forgot password
router.post("/forgotPassword", getOtpValidation, controller.genOtp);
router.post("/verifyOtp", tokenValidation, controller.verifyOtp);
router.post("/changePassword", controller.forgotPassword);
//register and login

// const admin = ( ) => {
//   if(req.body.userRole === 'admin') {
//     const { id } = req.params;
//     const data = await userAssigned
//       .find({})
//       .populate({ path: "assetNumber" })
//       .populate({ path: "employeeDetails", select: ["name", "status"] });
//       res,next()
//     return res.status(200).json({ message: data });
//   } else {

//     // unauthorized user.
//   }
// }
// router.post("/register", employeeValidation, isAdmin, controller.createEmployeeDetails);
router.post("/register", employeeValidation, controller.createEmployeeDetails);
router.post("/login", controller.loginDetails);
// update
router.patch(
  "/update/:id",
  tokenValidation,
  updateDetailsVerify,
  controller.updateDetails
);
//delete
router.delete("/delete/:id", tokenValidation, controller.deleteEmployee);

//status change
router.post("/status", tokenValidation, controller.statusData);
export default router;
