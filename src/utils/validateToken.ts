// import { NextFunction, Request, Response } from "express";
// import jwt from "jsonwebtoken";
// import Employee from "../models/employee.model";

// export const isEmployee = async (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   try {
//     const token = req.headers.token;
//     const verify = jwt.verify(`${token}`, process.env.KEY);
//     // const user = userModal.find({_id: verify.id})
//     // if(user && user.status === 'active') {
//     //   req.body.userRole = user.role;
//     //   req.body.userId = user.id;
//     // } else {
//     //   return res.status(404).json({ message: "User not present" });
//     // }
//     req.body.id = verify;
//   } catch (error) {
//     return res.status(400).json({ message: "Token required" });
//   }
//   next();
//};

import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

export const tokenValidation = (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.token;
    if (!token) {
      return res.status(400).json({ message: "Token required" });
    }
    const verify: any = jwt.verify(`${token}`, process.env.KEY);
    req.body.userId = verify.emailId;
    // req.body.id = verify;
    console.log("verify", verify);
  } catch (error) {
    return res.status(400).json({ message: "Invalid token" });
  }
  next();
};
