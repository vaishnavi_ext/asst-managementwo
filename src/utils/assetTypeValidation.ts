import { NextFunction, Request, Response } from "express";

export const assetTypeValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { name } = req.body;
  const errors = [];
  !name && errors.push({ message: "Name field is required!" });
  errors.length ? res.status(400).json({ status: false, errors }) : next();
};

// export const desktopValidation = (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   const {
//     serialNumber,
//     modelNumber,
//     modelName,
//     ram,
//     processor,
//     internalStorage,
//     keyboard,
//     mouse,
//   } = req.body;
//   const errors = [];
//   !serialNumber && errors.push({ message: "serial Number is required!" });
//   !modelNumber && errors.push({ message: "modelNumber is required" });
//   !modelName && errors.push({ message: "modelName is required" });
//   !ram && errors.push({ message: "RAM is required" });
//   !processor && errors.push({ message: "Processor is required" });
//   !internalStorage && errors.push({ message: "Internal Storage is required" });
//   !keyboard && errors.push({ message: "Keyboard data is required" });
//   !mouse && errors.push({ message: "Mouse data is required" });
//   errors.length ? res.status(400).json({ status: false, errors }) : next();
// };

// export const mobileValidation = (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   const {
//     serialNumber,
//     imeinumber,
//     modelName,
//     ram,
//     mobileOS,
//     internalStorage,
//   } = req.body;
//   const errors = [];
//   !serialNumber && errors.push({ message: "serial Number is required!" });
//   !imeinumber && errors.push({ message: "IMEI number is required" });
//   if (imeinumber.length > 15 || imeinumber.length < 15) {
//     errors.push({ message: "IMEI should be 15 digits" });
//   }
//   !modelName && errors.push({ message: "modelName is required" });
//   !ram && errors.push({ message: "RAM is required" });

//   !internalStorage && errors.push({ message: "Internal Storage is required" });
//   errors.length ? res.status(400).json({ status: false, errors }) : next();
// };
