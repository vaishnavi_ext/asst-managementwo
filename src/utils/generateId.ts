// import idsModel from "../models/ids.model";
// import {
//   CUSTOMER_ID_LENGTH,
//   CUSTOMER_PREFIX,
//   ORDER_ID_LENGTH,
//   ORDER_PREFIX,
//   PRODUCT_ID_LENGTH,
//   PRODUCT_PREFIX,
//   SELLER_ID_LENGTH,
//   SELLER_PREFIX,
// } from "./config";

// const generateId = async (
//   type: "ORDER" | "PRODUCT" | "SELLER" | "CUSTOMER"
// ) => {
//   const ids = await idsModel.find();
//   let orderCount = 0;
//   let productCount = 0;
//   let sellerCount = 0;
//   let customerCount = 0;

//   if (ids.length === 0) {
//     const instance = new idsModel({
//       orderId: 1,
//       productId: 1,
//       sellerId: 1,
//       customerId: 1,
//     });
//     const initalCount = await instance.save();
//     orderCount = initalCount.orderId;
//     productCount = initalCount.productId;
//     sellerCount = initalCount.sellerId;
//     customerCount = initalCount.customerId;
//   } else {
//     orderCount = ids[0].orderId + 1;
//     productCount = ids[0].productId + 1;
//     sellerCount = ids[0].sellerId + 1;
//     customerCount = ids[0].customerId + 1;
//   }

//   if (type === "ORDER") {
//     if (ids.length > 0) {
//       if (ids[0].orderId) {
//         ids[0].orderId = orderCount;
//         ids[0].save();
//       }
//     }
//     const strIndex = orderCount.toString().padStart(ORDER_ID_LENGTH, "0");
//     const orderId = ORDER_PREFIX + strIndex;
//     return orderId;
//   } else if (type === "PRODUCT") {
//     if (ids.length > 0) {
//       if (ids[0].productId) {
//         ids[0].productId = productCount;
//         ids[0].save();
//       }
//     }
//     const strIndex = productCount.toString().padStart(PRODUCT_ID_LENGTH, "0");
//     const productId = PRODUCT_PREFIX + strIndex;
//     return productId;
//   } else if (type === "SELLER") {
//     if (ids.length > 0) {
//       if (ids[0].sellerId) {
//         ids[0].sellerId = sellerCount;
//         ids[0].save();
//       }
//     }
//     const strIndex = sellerCount.toString().padStart(SELLER_ID_LENGTH, "0");
//     const sellerId = SELLER_PREFIX + strIndex;
//     return sellerId;
//   } else {
//     if (ids.length > 0) {
//       if (ids[0].customerId) {
//         ids[0].customerId = customerCount;
//         ids[0].save();
//       }
//     }
//     const strIndex = customerCount.toString().padStart(CUSTOMER_ID_LENGTH, "0");
//     const customerId = CUSTOMER_PREFIX + strIndex;
//     return customerId;
//   }
// };

// export default generateId;
