import { Response, Request, NextFunction } from "express";
import EmployeeSchema from "../models/employee.model";

export const isUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers.token;
    const id = req.body.userId;
    console.log("userid-", id);
    const findUser: any = await EmployeeSchema.findOne({ employeeId: id });
    if (
      findUser.role !== "admin" &&
      findUser.role !== "it" &&
      findUser.role !== "employee" &&
      findUser.role !== "manager"
    ) {
      return res.status(400).json({ message: "unauthorised user" });
    } else {
      next();
    }
  } catch (error) {
    return res.status(200).json({ message: error.message });
  }
};

export const isIT = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.token;
    const id = req.body.userId;
    console.log("userid-", id);
    const findUser: any = await EmployeeSchema.findOne({ employeeId: id });
    if (
      findUser.role !== "admin" &&
      findUser.role !== "manager" &&
      findUser.role !== "employee"
    ) {
      return res.status(400).json({ message: "unauthorised user" });
    } else {
      next();
    }
  } catch (error) {
    return res.status(200).json({ message: error.message });
  }
};

export const isManager = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const token = req.headers.token;
    const id = req.body.userId;
    console.log("userid-", id);
    const findUser: any = await EmployeeSchema.findOne({ employeeId: id });
    if (findUser.role === "employee") {
      return res.status(400).json({ message: "unauthorised user" });
    } else {
      next();
    }
  } catch (error) {
    return res.status(200).json({ message: error.message });
  }
};

export const isAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const id = req.body.userId;
    console.log("userid-", id);
    const findUser: any = await EmployeeSchema.findOne({ employeeId: id });
    if (findUser.role !== "admin") {
      return res.status(400).json({ message: "unauthorised user" });
    } else {
      next();
    }
  } catch (error) {
    return res.status(200).json({ message: error.message });
  }
};
