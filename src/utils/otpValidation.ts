import { NextFunction, Request, Response } from "express";
import employee from "models/employee.model";

export const getOtpValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { emailId } = req.body;
  const errors = [];

  if (!emailId) {
    errors.push({ message: "email Id is required" });
  }
  errors.length ? res.status(400).json({ status: "false", errors }) : next();
};

export const loginValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { emailId, otp } = req.body;
  const errors = [];

  if (!emailId) {
    errors.push({ message: "emailId is required" });
  } else if (typeof emailId !== "string") {
    errors.push({ message: "emailId must be string" });
  } else if (emailId && !/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(emailId)) {
    errors.push({ message: "email is not valid" });
  }
  if (!otp) {
    errors.push({ message: "otp field is required" });
  }

  errors.length ? res.status(400).json({ status: "false", errors }) : next();
};

// export const updateProfileValidation = (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   const {
//     firstname,
//     lastname,
//     email,
//     address,
//     gender,
//     profilePic,
//     device_token,
//     device_type,
//     dob,
//   } = req.body;
//   const errors = [];

//   firstname &&
//     typeof firstname !== "string" &&
//     errors.push({ message: "firstname should be string" });
//   lastname &&
//     typeof lastname !== "string" &&
//     errors.push({ message: "lastname should be string" });

//   if (email && !/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(email)) {
//     errors.push({ message: "email is not valid" });
//   }

//   address &&
//     typeof address !== "string" &&
//     errors.push({ message: "address should be string" });

//   if (gender && gender !== "male" && gender !== "female") {
//     errors.push({ message: "gender field should be contains male or female" });
//   }

//   profilePic &&
//     typeof profilePic !== "string" &&
//     errors.push({ message: "profilePic url should be string" });

//   device_token &&
//     typeof device_token !== "string" &&
//     errors.push({ message: "device_token should be string" });

//   device_type &&
//     typeof device_type !== "string" &&
//     errors.push({ message: "device_type should be string" });

//   dob &&
//     typeof dob !== "string" &&
//     errors.push({ message: "dob should be string" });

//   errors.length ? res.status(400).json({ status: "false", errors }) : next();
// };
