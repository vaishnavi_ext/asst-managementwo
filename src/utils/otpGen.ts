// import smsClient from "twilio";
// const id = "ACdd99fe8acdf5eb2f0041238ef1388f0e";
// const token = "e691cd8a4afabb7f17dd0ba795100b5b";
// const client = smsClient(id, token);

// export const generateOtp = () => {
//   const otp = Math.floor(1000 + Math.random() * 9000);
//   return otp;
// };

// export const getOtp = async (mobile: Number) => {
//   try {
//     const otp = Math.floor(1000 + Math.random() * 9000);
//     const response = await client.messages.create({
//       body: `${otp}`,
//       to: `+91${mobile}`,
//       from: "16696006949",
//     });
//     return response.body.substring(38, 42);
//   } catch (error) {
//     return error.message;
//   }
// };

// const accountSid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'; // Your Account SID from www.twilio.com/console
// const authToken = 'your_auth_token'; // Your Auth Token from www.twilio.com/console

// const twilio = require('twilio');
// const client = new twilio(accountSid, authToken);

// client.messages
//   .create({
//     body: 'Hello from Node',
//     to: '+12345678901', // Text this number
//     from: '+12345678901', // From a valid Twilio number
//   })
//   .then((message) => console.log(message.sid));

// import smsClient from "twilio";
// const client = smsClient(
//   "ACa081f7c439435784fdc54fdeee9c2060",
//   "dbbbfaed0327d2b83732823ad9559c72"
// );

// export const generateOtp = () => {
//   const otp = Math.floor(1000 + Math.random() * 9000);
//   return otp;
// };

// export const getOtp = async (otp: Number, mobile: Number) => {
//   const response = await client.messages.create({
//     body: `Your OTP is ${otp}`,
//     to: `+91${mobile}`,
//     from: "+16075588497",
//   });
//   return response;
// };

const nodemailer = require("nodemailer");
const dotenv = require("dotenv");
const { google } = require("googleapis");
dotenv.config();

const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2(
  process.env.OAUTH_CLIENTID,
  process.env.OAUTH_CLIENT_SECRET,
  "https://developers.google.com/oauthplayground" // Redirect URL
);
oauth2Client.setCredentials({
  refresh_token: process.env.OAUTH_REFRESH_TOKEN,
});
const accessToken = oauth2Client.getAccessToken();

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    type: "OAuth2",
    user: "vaishnavi.vadlamudi@extwebtech.in", // generated ethereal user
    pass: process.env.PASSWORD,
    clientId: process.env.OAUTH_CLIENTID,
    clientSecret: process.env.OAUTH_CLIENT_SECRET,
    refreshToken: process.env.OAUTH_REFRESH_TOKEN,
    accessToken: accessToken,
  },
});

export const mailer = (emailId: String, otp: Number) => {
  let mailOptions = {
    from: "assetmanagementext@gmail.com", // sender address
    to: `${emailId}`, // list of receivers
    subject: "Verification Code",
    html: `
    <body>
    <div style="background-color: #e5f2ff;  height: 322px;" >
        <div style="background-color: #F6FAFF; 
        border: 1px solid #fafafa;
        height: 300px;
        width: 600px;
        position: relative;
        border-radius: 5px;
        padding: 20px;
        margin: auto;
        top:0%;
        bottom: 0%;
        left: 0%;
        right: 0%;">
        <h1>Hi! ${
          emailId.split(".")[0].charAt(0).toUpperCase() +
          emailId.slice(1).split(".")[0]
        },</h1>
            <p style=" font-family: 'Montserrat', sans-serif; ">We have received a request to reset your password.</p>
            <p style=" font-family: 'Montserrat', sans-serif; ">Here is the verification code: </p>
            <h3 style="color: #0C99EB;"><strong style=" font-family: 'Montserrat', sans-serif ">${otp}<strong></h3>
            <p style="text-align:right;">-Team ExtWebTech</p>
        </div>
        <div>
    </body>`,
  };
  transporter.sendMail(mailOptions, function (error: any, info: any) {
    if (error) {
      console.log(error.message);
    }
    console.log("Email Sent: " + info);
  });
};
// mailer("vaishnavi.vadlamudi@extwebtech.in", 1234);
