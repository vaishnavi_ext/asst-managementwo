import { NextFunction, Request, Response } from "express";
import * as val from "../utils/validation";

export const employeeValidation = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { employeeId, password, confirmPassword, emailId } = req.body;
  const errors = [];
  !val.isValid(employeeId) &&
    errors.push({ message: "employeeId field is required" });
  !val.isValidPassword(password) &&
    errors.push({ message: "password field is required" });
  !val.isValidPassword(confirmPassword) &&
    errors.push({ message: "password field is required" });
  !val.isValidEmail(emailId) && errors.push({ message: "email is required!" });
  // !val.isValidPhone(mobileNumber) &&
  //   errors.push({ message: "Mobile number is required" });
  // if (mobileNumber.length < 10 || mobileNumber.length > 10) {
  //   errors.push({ message: "Mobile Number should have 10 digits" });
  // }

  errors.length ? res.status(400).json({ status: "false", errors }) : next();
};

export const updateDetailsVerify = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    employeeId,
    password,
    confirmPassword,
    emailId,
    address,
    shippingAddress,
    role,
    location,
    mobileNumber,
  } = req.body;
  const errors = [];
  val.isValid(employeeId) &&
    errors.push({ message: "employeeId cannot be modified" });
  !val.isValidPassword(password) &&
    errors.push({ message: "password field is required" });
  !val.isValidPassword(confirmPassword) &&
    errors.push({ message: "password field is required" });
  if (password !== confirmPassword) {
    errors.push({ message: "passwords not matching" });
  }
  if (password.length < 8 && confirmPassword.length < 8) {
    errors.push({ message: "password should be atleast 8 chars" });
  }
  !val.isValidEmail(emailId) && errors.push({ message: "email is required!" });
  !address && errors.push({ message: "Address is required!" });
  !shippingAddress && errors.push({ message: "Shipping address is required!" });
  role && errors.push({ message: "Role cannot be modified!" });
  !location && errors.push({ message: "Location is required!" });
  !val.isValidPhone(mobileNumber) &&
    errors.push({ message: "Mobile number is required" });
  if (mobileNumber.length < 10 || mobileNumber.length > 10) {
    errors.push({ message: "Mobile Number should have 10 digits" });
  }
  errors.length ? res.status(400).json({ status: "false", errors }) : next();
};
// export const desktopValidation = (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   const {
//     serialNumber,
//     modelNumber,
//     modelName,
//     ram,
//     processor,
//     internalStorage,
//     keyboard,
//     mouse,
//   } = req.body;
//   const errors = [];
//   !serialNumber && errors.push({ message: "serial Number is required!" });
//   !modelNumber && errors.push({ message: "modelNumber is required" });
//   !modelName && errors.push({ message: "modelName is required" });
//   !ram && errors.push({ message: "RAM is required" });
//   !processor && errors.push({ message: "Processor is required" });
//   !internalStorage && errors.push({ message: "Internal Storage is required" });
//   !keyboard && errors.push({ message: "Keyboard data is required" });
//   !mouse && errors.push({ message: "Mouse data is required" });
//   errors.length ? res.status(400).json({ status: false, errors }) : next();
// };

// export const mobileValidation = (
//   req: Request,
//   res: Response,
//   next: NextFunction
// ) => {
//   const {
//     serialNumber,
//     imeinumber,
//     modelName,
//     ram,
//     mobileOS,
//     internalStorage,
//   } = req.body;
//   const errors = [];
//   !serialNumber && errors.push({ message: "serial Number is required!" });
//   !imeinumber && errors.push({ message: "IMEI number is required" });
//   if (imeinumber.length > 15 || imeinumber.length < 15) {
//     errors.push({ message: "IMEI should be 15 digits" });
//   }
//   !modelName && errors.push({ message: "modelName is required" });
//   !ram && errors.push({ message: "RAM is required" });

//   !internalStorage && errors.push({ message: "Internal Storage is required" });
//   errors.length ? res.status(400).json({ status: false, errors }) : next();
// };
